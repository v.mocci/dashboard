package excel

import (
	"fmt"
	"encoding/json"
	"time"
	"github.com/360EntSecGroup-Skylar/excelize"
)

type Recharge struct {
	UserEmail       string    `gorm:"column:useremail"`
	CardNumber      string    `gorm:"column:cardnumber"`
	MundipaggID     string    `gorm:"column:mundipaggid"`
	MundipaggStatus string    `gorm:"column:mundipaggstatus"`
	KondutoStatus   string    `gorm:"column:kondutostatus"`
	TacomOrderID    string    `gorm:"column:tacomorderid"`
	RechargeValue   float64   `gorm:"column:rechargevalue"`
	CreatedAt       time.Time `gorm:"column:createdAt"`
}

const (
	dateFormat = "02-01-2006"
)

//Create function - cria um arquivo xlsx para Excel
func Create(jsonData []byte) {

	cellFormat := [7]string{"A", "B", "C", "D", "E", "F", "G"}
	cellValue  := [7]string{"Email Pedido", "Cartão BHBus", "Pedido Tacom", "Valor de Recarga", "Data", "Status Anti-fraude", "Status Pagamento"}

	c := []Recharge{}

	err := json.Unmarshal([]byte(jsonData), &c)
	if err != nil {
		fmt.Println(err)
	}

	f := excelize.NewFile()
	// Create a new sheet.
	index := f.NewSheet("Sheet1")
	// Set value of a cell.
	for j := 0; j <= 6; j++ {
		f.SetCellValue("Sheet1", fmt.Sprintf("%s1", cellFormat[j]), cellValue[j])
	}
	for i := 0; i < len(c); i++ {

		if c[i].MundipaggID == "" {
			c[i].MundipaggID = "-"
		}
		if c[i].TacomOrderID == "" {
			c[i].TacomOrderID = "-"
		}

		switch c[i].MundipaggStatus {
		case "order.payment_failed":
			c[i].MundipaggStatus = "não pago"
		case "canceled":
			c[i].MundipaggStatus = "pagamento cancelado"
		case "pending":
			c[i].MundipaggStatus = "pendente"
		case "approved", "order.paid":
			c[i].MundipaggStatus = "pago"
		default:
			c[i].MundipaggStatus = "-"
		}

		switch c[i].KondutoStatus {
		case "declined", "DECLINE":
			c[i].KondutoStatus = "Negado"
		case "canceled":
			c[i].KondutoStatus = "Cancelado"
		case "fraud":
			c[i].KondutoStatus = "Fraude"
		case "pending":
			c[i].KondutoStatus = "Pendente"
		case "approved", "APPROVE":
			c[i].KondutoStatus = "Aprovado"
		case "not_authorized":
			c[i].KondutoStatus = "Pagamento não autorizado"
		default:
			c[i].KondutoStatus = "-"
		}

		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i+2), c[i].UserEmail)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i+2), c[i].CardNumber)
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i+2), c[i].TacomOrderID)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i+2), fmt.Sprintf("%.2f", c[i].RechargeValue/100))
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i+2), c[i].CreatedAt.Format(dateFormat))
		f.SetCellValue("Sheet1", fmt.Sprintf("F%d", i+2), c[i].KondutoStatus)
		f.SetCellValue("Sheet1", fmt.Sprintf("G%d", i+2), c[i].MundipaggStatus)
	}
	// Set active sheet of the workbook.
	f.SetActiveSheet(index)
	// Save xlsx file by the given path.
	//fileName := time.Now().Format(dateFormat)
	
	if err := f.SaveAs("./assets/export/Pedidos.xlsx"); err != nil {
		println(err.Error())
	}
}
