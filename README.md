## About

Dashboard created to show my abilities.

## Technologies

- Go;
- HTML;
- JavaScript;
- CSS/Boostrap;
- SQL/Postgres (flexible to use MySql too);

## OS

The system has a mecanism to run in Linux, Mac OS and Windows. It's possible to use a continuous program runner, the "dashboard-server.service" file. It prevents the system getting offline in Linux systems even if the PC shuts down.

## Screenshots

![Image description](assets/img/screenshot-01.png)
![Image description](assets/img/screenshot-02.png)
![Image description](assets/img/screenshot-03.png)