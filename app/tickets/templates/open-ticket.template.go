package opentickettemplate

//CCOrderSearchTemplate stores the template
func TicketOpenTemplate(tpl string) string {

	var tickettemplate string

	switch tpl {
	case "htmltemplate1":
		tickettemplate = `
		
			<!DOCTYPE html>
			<html>
			
			<head>
			  <!-- Required meta tags -->
			  <meta charset="utf-8">
			  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			
			  <!-- Bootstrap CSS -->
			  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
				integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
			
			  <!-- Internal CSS -->
			  <link rel="stylesheet" href="/assets/css/style.css">
			   
			  <!-- Tab Icon -->
			  <link rel="icon" type="image/png" href="/../assets/img/vm-logo.png">

			  <!-- Title -->
			  <title>Dashboard BHBus</title>

			</head>
			
			
			<body class="bodyConfig">
			
			  <div class="container-fullwidth">
				<!-- NavBar start-->
				<nav class="navbar navbar-expand-lg header">
				  
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
			    
				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				  	<h5 class="text-white pt-2"> Parabéns! </h5>
				  </div>
				  </nav>
				<!-- NavBar end-->
			
			<div class="container-fluid">	`

	case "htmltemplate2":
		tickettemplate = `
		    </div>
		     </div>
		     <br>

		

			 <center> <p> Você abriu um chamado com sucesso! <br> Por gentileza aguarde o contato do suporte em até 2 dias úteis. </p> </center>
			 <center>
			 <a href="/tickets/"> <button type="submit" class="btn text-white btnSearch btn-primary header">Abrir outro chamado</button></a>
			 <a href="/order/"> <button type="submit" class="btn text-white btnSearch btn-primary header">Voltar para a Home</button> </a>
			 </center>
		    
			</body>
			</html> `

	default:
		tickettemplate = ""
	}

	return tickettemplate

}
