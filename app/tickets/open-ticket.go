package opentickets

import (
	"fmt"
	"net/http"
	"runtime"
	"dashboard-portfolio/dashboard/connection"
	"dashboard-portfolio/dashboard/app/tickets/templates"
)

//Ticket define a estrutura do chamado
type Ticket struct {
	ID                int64  `gorm:"PRIMARY_KEY"`
	TicketNumber      string `gorm:"column:ticketNumber"`
	Title             string `gorm:"column:title"`
	Requester         string `gorm:"column:requester"`
	RequesterEmail    string `gorm:"column:requesterEmail"`
	RequesterTel      string `gorm:"column:requesterTel"`
	TicketCategory    string `gorm:"column:ticketCategory"`
	TicketPriority    string `gorm:"column:ticketPriority"`
	TicketStatus      string `gorm:"column:ticketStatus"`
	TicketIssueDate   string `gorm:"column:ticketIssueDate"`
	TicketDescription string `gorm:"column:ticketDescription"`
}

//CreateNewTicket mostra o resultado da pesquisa por pedido em cartão de crédito
func CreateNewTicket(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":
		//Verifica o sistema operacional para definir a rota
		switch os := runtime.GOOS; os {
		case "darwin", "windows":
			http.ServeFile(w, r, "app/tickets/open-ticket.html")
		case "linux":
			http.ServeFile(w, r, "opt/go/dash-anti-fraude/new-dashboard-bhbus/app/tickets/aviso.html")
		default:
			fmt.Fprintf(w, "Check your OS version, the path for Credit Card Orders module is not set.")
		}

	case "POST":

		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		inputTitle := r.PostFormValue("title")
		inputRequester := r.PostFormValue("requester")
		inputRequesterEmail := r.PostFormValue("requesterEmail")
		inputRequesterTel := r.PostFormValue("requesterTel")
		inputCategory := r.PostFormValue("categoryOptions")
		inputPriority := r.PostFormValue("priorityOptions")
		inputDescription := r.PostFormValue("#ticketDescription")
		//inputIssueDate := r.PostFormValue("ticketIssueDate")
		db, _ := connection.ConnecTest()

		ticket := Ticket{

			TicketNumber:      "2",
			Title:             inputTitle,
			Requester:         inputRequester,
			RequesterEmail:    inputRequesterEmail,
			RequesterTel:      inputRequesterTel,
			TicketCategory:    inputCategory,
			TicketPriority:    inputPriority,
			TicketStatus:      "Pendente análise",
			TicketIssueDate:   "2020-02-18 00:00:00",
			TicketDescription: inputDescription,
		}
		db.Table("tickets").Create(&ticket)
		db.Last(&ticket)
		fmt.Println(ticket)
		fmt.Fprintf(w, "%v", opentickettemplate.TicketOpenTemplate("htmltemplate1"))
		fmt.Fprintf(w, "%v", opentickettemplate.TicketOpenTemplate("htmltemplate2"))

	default:
		fmt.Fprintf(w, "Houve um problema ao tentar acessar a página solicitada, por gentileza entre em contato com o suporte pelo menu Sobre o Dashboard BHBus.")
	}

}
