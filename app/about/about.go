package siteabout

import (
	"fmt"
	"html/template"
	"net/http"
	"runtime"
)

type SiteAboutPage struct {
	Title, Description, Contact string
}

func AboutPage(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":

		//Verifica o sistema operacional para definir a rota
		switch os := runtime.GOOS; os {
		case "darwin", "windows":
			p := SiteAboutPage{Title: "About Us:", Description: "Administrative Dashboard 2.0 Beta by Vinicius Mocci.", Contact: "vinicius.mocci@hotmail.com"}
			t, err := template.ParseFiles("app/about/about.html")
			fmt.Println(err)
			t.Execute(w, p)
		case "linux":
			p := SiteAboutPage{Title: "About Us:", Description: "Administrative Dashboard 2.0 Beta by Vinicius Mocci.", Contact: "vinicius.mocci@hotmail.com"}
			t, err := template.ParseFiles("opt/go/dash-anti-fraude/new-dashboard-bhbus/app/about/about.html")
			fmt.Println(err)
			t.Execute(w, p)
		default:
			p := SiteAboutPage{Title: "About Us:", Description: "Administrative Dashboard 2.0 Beta by Vinicius Mocci.", Contact: "vinicius.mocci@hotmail.com"}
			t, err := template.ParseFiles("app/about/about.html")
			fmt.Println(err)
			t.Execute(w, p)
		}

	case "POST":

	}

}
