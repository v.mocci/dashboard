package usersearch

import (
	"fmt"
	"net/http"
	"runtime"
	"time"

	usersearchtemplate "dashboard-portfolio/dashboard/app/user/templates"
					   "dashboard-portfolio/dashboard/connection"
)

type searchUser struct {
	ID                  int64     `gorm:"PRIMARY_KEY"`
	FirstName           string    `gorm:"column:firstname"`
	LastName            string    `gorm:"column:lastname"`
	Email               string    `gorm:"column:email"`
	Type           		string    `gorm:"column:type"`
	CreatedAt           time.Time `gorm:"column:createdAt"`
	UpdatedAt           string    `gorm:"column:updatedAt"`
	Birthday            string    `gorm:"column:birthday"`
	Document            string    `gorm:"column:document"`
	Phone               string    `gorm:"column:phone"`
		
}

//Time constant formatation
const (
	dateFormat = "02-01-2006 15:04:05"
)

//Results shows the result for user search
func Results(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":
		//Verifies the OS to define routes
		switch os := runtime.GOOS; os {
		case "darwin", "windows":
			http.ServeFile(w, r, "app/user/user-search.html")
		case "linux":
			http.ServeFile(w, r, "Check your OS version, the path for Credit Card Orders module is not set.")
		default:
			fmt.Fprintf(w, "Check your OS version, the path for Credit Card Orders module is not set.")
		}

	case "POST":
		// Cal Parse Form to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		var inputValue 		   string
		var inputValueOption   string
		db, _ 				  := connection.ConnecTest()
		inputValue 			   = r.PostFormValue("user")
		inputValueOption	   = r.PostFormValue("options")

		defer db.Close()

		c := []searchUser{}

		db.Table("user").Where(fmt.Sprintf("%s LIKE ?", inputValueOption), inputValue).Find(&c)

		if len(c) <= 0 {
			fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("htmltemplate1"))
			fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("htmltemplate3"))
			fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("htmltemplate2"))
			fmt.Println("There is no results for this filter.")
		} else {

			fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("htmltemplate1"))
			if len(c) >= 20 {
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletemplate2"))
			} else {
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletemplate1"))
			}

			for i := 0; i < len(c); i++ {

				IDUser 		    := c[i].ID
				FirstNameUser   := c[i].FirstName
				LastNameUser    := c[i].LastName
				FullNameUser    := FirstNameUser + " " + LastNameUser
				EmailUser	    := c[i].Email
				TypeUser 		:= c[i].Type
				PhoneUser 	    := c[i].Phone
				BirthdayUser    := c[i].Birthday
				DocumentUser    := c[i].Document
				CreatedAtUser   := c[i].CreatedAt

				fmt.Fprintf(w, "%v \t", usersearchtemplate.UserSearchTemplate("tabletrbeggin"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", IDUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", FullNameUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", EmailUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))
				
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", TypeUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", PhoneUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", BirthdayUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", DocumentUser)
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", CreatedAtUser.Format(dateFormat))
				fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "\n")

			}

			fmt.Fprintf(w, "%v ", usersearchtemplate.UserSearchTemplate("tabletrend"))
			fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("tabletemplateend"))

			fmt.Fprintf(w, "%v", usersearchtemplate.UserSearchTemplate("htmltemplate2"))

		}

	default:
		fmt.Fprintf(w, "There was a problem trying to acess the page, please contact the support team: vinicius.mocci@hotmail.com")
	}

}
