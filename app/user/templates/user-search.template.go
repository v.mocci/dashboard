package usersearchtemplate

//UserSearchTemplate shows the user template
func UserSearchTemplate(tpl string) string {

	var usertemplate string

	switch tpl {
	case "htmltemplate1":
		usertemplate = `
		
			<!DOCTYPE html>
			<html>
			
			<head>
			  <!-- Required meta tags -->
			  <meta charset="utf-8">
			  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			
			  <!-- Bootstrap CSS -->
			  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
				integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
			
			  <!-- Internal CSS -->
			  <link rel="stylesheet" href="/assets/css/style.css">
			   
			  <!-- Tab Icon -->
			  <link rel="icon" type="image/png" href="/../assets/img/vm-logo.png">

			  <!-- Title -->
			  <title>Dashboard BHBus</title>

			</head>
			
			
			<body class="bodyConfig">
			
			  <div class="container-fullwidth">
				<!-- NavBar start-->
				<nav class="navbar navbar-expand-lg header">
				  
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
			    
				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				  <h5 class="text-white pt-2"> Results - Users </h5>
				  </div>

				</nav>
				<!-- NavBar end-->
			
			<div class="container-fluid">	`

	case "htmltemplate2":
		usertemplate = `
            </div>
             </div>
                 		 
              <script>
			  
			  //Get the button:
                mybutton = document.getElementById("myBtn");
                 
              //When the user scrolls down 20px from the top of the document, show the button
                window.onscroll = function() {scrollFunction()};
                 
                function scrollFunction() {
                   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                     mybutton.style.display = "block";
                   } else {
                     mybutton.style.display = "none";
                   }
                 }
                 
              //When the user clicks on the button, scroll to the top of the document
                function topFunction() {
                   document.body.scrollTop = 0; // For Safari
                   document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                 }
                 			 
			   </script>
			   
			   <script>
	             function goBack() {
	             	window.history.back()
	             }
	             </script>
                 
             <button onclick="topFunction()" id="searchResultsBtn" title="Go to top">Top</button>
              <br>
               <center><button type="submit" onclick="goBack()" class="btn text-white btnSearch btn-primary header">Go Back</button></center>
                <br><br><br>
            <div class="footer">
              <br>
                <p>Copyright 2020, by Vinicius Mocci.</p>
            </div>
                 
			</body>
			</html> `

	case "htmltemplate3":
		usertemplate = `
				</div>
				 </div>
			     <br>
	
				 <p> There is no results for this filter. </p>
				
				</body>
				</html> `

	case "tabletemplate1":
		usertemplate = `
			<br>
			<table class="loadPageEffect tableStyle" style="width:100%">
			<tr>
			     <th class="tableBackground"> User ID 			   </th>
			     <th class="tableBackground"> Name		 		   </th>
				 <th class="tableBackground"> E-mail		       </th>
				 <th class="tableBackground"> Type		 		   </th>
				 <th class="tableBackground"> Telephone 		   </th>
				 <th class="tableBackground"> Birthday  		   </th>
				 <th class="tableBackground"> Document		  	   </th>
			     <th class="tableBackground"> Created At		   </th>
			</tr>
			`
	case "tabletemplate2":
		usertemplate = `
             
	             <script>
	             function goBack() {
	             	window.history.back()
	             }
	             </script>
				<br>
				<center><button type="submit" class="btn text-white onclick="goBack()" btnSearch btn-primary header">Go Back</button></center>
			<br>
			<table class="loadPageEffect tableStyle" style="width:100%">
			<tr>
			     <th class="tableBackground"> User ID 			   </th>
			     <th class="tableBackground"> Name		 		   </th>
				 <th class="tableBackground"> E-mail		       </th>
				 <th class="tableBackground"> Type		 		   </th>
				 <th class="tableBackground"> Telephone 		   </th>
				 <th class="tableBackground"> Birthday  		   </th>
				 <th class="tableBackground"> Document		  	   </th>
			     <th class="tableBackground"> Created At		   </th>
			</tr>
			
			`
	case "tabletrbeggin":
		usertemplate = `<tr class="tableBackground">`

	case "tabletrend":
		usertemplate = `</tr>`

	case "tabletdbeggin":
		usertemplate = `<td class="tableBackground">`

	case "tabletdend":
		usertemplate = `</td>`

	case "tabletemplateend":
		usertemplate = `</table>`

	default:
		usertemplate = ""

	}
	return usertemplate
}
