package cardsearch

import (
	"fmt"
	"net/http"
	"runtime"

	cardsearchtemplate "dashboard-portfolio/dashboard/app/card/templates"
					   "dashboard-portfolio/dashboard/connection"
)

type searchCard struct {
	ID       int64  `gorm:"PRIMARY_KEY"`
	Number   string `gorm:"column:number"`
	Nickname string `gorm:"column:nickname"`
	User     string `gorm:"column:user"`
}

//CardSearchFunc shows the card search results
func CardSearchFunc(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":
		//Verifies the OS to define routes
		switch os := runtime.GOOS; os {
		case "darwin", "windows":
			http.ServeFile(w, r, "app/card/card-search.html")
		case "linux":
			http.ServeFile(w, r, "opt/go/dash-anti-fraude/new-dashboard-bhbus/app/card/card-search.html")
		default:
			fmt.Fprintf(w, "Check your OS version, the path for Credit Card Orders module is not set.")
		}

	case "POST":
		// Cal Parse Form to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		var inputValue string
		var inputValueOption string
		db, _ := connection.ConnecTest()
		inputValue = r.PostFormValue("user")
		inputValueOption = r.PostFormValue("options")

		defer db.Close()

		c := []searchCard{}

		db.Table("card").Where(fmt.Sprintf("%s = ?", inputValueOption), inputValue).Find(&c)

		if len(c) <= 0 {
			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("htmltemplate1"))
			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("htmltemplate3"))
			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("htmltemplate2"))
			fmt.Println("There is no results for this filter.")
		} else {

			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("htmltemplate1"))

			if len(c) >= 20 {
				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletemplate2"))
			} else {
				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletemplate1"))
			}

			for i := 0; i < len(c); i++ {

				IDCard := c[i].ID
				NumberCard := c[i].Number
				NicknameCard := c[i].Nickname
				IDUserCard := c[i].User

				fmt.Fprintf(w, "%v \t", cardsearchtemplate.CardSearchTemplate("tabletrbeggin"))

				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", IDCard)
				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", NumberCard)
				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", NicknameCard)
				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", IDUserCard)
				fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "\n")

			}

			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletrend"))
			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("tabletemplateend"))

			fmt.Fprintf(w, "%v", cardsearchtemplate.CardSearchTemplate("htmltemplate2"))

		}

	default:
		fmt.Fprintf(w, "There was a problem trying to acess the page, please contact the support team: vinicius.mocci@hotmail.com")
	}

}
