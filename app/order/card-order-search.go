package ordersearch

import (
	"encoding/json"
	"fmt"
	"net/http"
	"runtime"
	"time"

	ordersearchtemplate "dashboard-portfolio/dashboard/app/order/templates"
						"dashboard-portfolio/dashboard/connection"
				  excel	"dashboard-portfolio/dashboard/excelExport"
)

type rechargeOrder struct {
	ID                    int64 `gorm:"PRIMARY_KEY"`
	Identifier            string
	UserEmail             string  `gorm:"column:useremail"`
	CardNumber            string  `gorm:"column:cardnumber"`
	HasPayment            bool    `gorm:"column:haspayment"`
	MundipaggID           string  `gorm:"column:mundipaggid"`
	MundipaggCode         string  `gorm:"column:mundipaggcode"`
	MundipaggStatus       string  `gorm:"column:mundipaggstatus"`
	MundipaggWebHookID    string  `gorm:"column:mundipaggwebhookid"`
	KondutoStatus         string  `gorm:"column:kondutostatus"`
	KondutoRecommendation string  `gorm:"column:kondutorecommendation"`
	TacomOrderID          string  `gorm:"column:tacomorderid"`
	RechargeValue         float64 `gorm:"column:rechargevalue"`
	Tax                   float64
	Amount                float64
	CreatedAt             time.Time `gorm:"column:createdAt"`
	UpdatedAt             string    `gorm:"column:updatedAt"`
}

//Formatação costante de tempo
const (
	//dateFormat = "02-01-2006"
	dateFormat = "02-01-2006 15:04:05" // <- string com horário para exibição
)

//CCOrderSearch mostra o resultado da pesquisa por pedido em cartão de crédito
func CCOrderSearch(w http.ResponseWriter, r *http.Request) {

	var MundipaggStatus string

	switch r.Method {
	case "GET":
		//Verifica o sistema operacional para definir a rota
		switch os := runtime.GOOS; os {
		case "darwin", "windows":
			http.ServeFile(w, r, "app/order/card-order-search.html")
		case "linux":
			http.ServeFile(w, r, "opt/go/dash-anti-fraude/new-dashboard-bhbus/app/order/card-order-search.html")
		default:
			fmt.Fprintf(w, "Check your OS version, the path for Credit Card Orders module is not set.")
		}

	case "POST":
		//Call ParseForm to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		var inputValue string
		var inputValueOption string
		var inputDateBegin string
		var inputDateEnd string
		db, _ := connection.ConnecTest()
		inputValue = r.PostFormValue("card")
		inputValueOption = r.PostFormValue("options")
		inputDateBegin = r.PostFormValue("datebegin")
		inputDateEnd = r.PostFormValue("dateend")
		timeInit := "'" + inputDateBegin + " 00:00:00'"
		timeEnd := "'" + inputDateEnd + " 23:59:59'"
		defer db.Close()

		c := []rechargeOrder{}

		tx := db.Table("mundipagg_order").Where(fmt.Sprintf("%s LIKE ?", inputValueOption), inputValue+"%").Order(`"createdAt" desc`).Find(&c)

		if inputDateBegin != "" && inputDateEnd != "" {
			tx = tx.Where(`"createdAt" BETWEEN ? AND ?`, timeInit, timeEnd).Order(`"createdAt" desc`).Find(&c)
		} else if inputDateBegin != "" {
			inputDateEnd = inputDateBegin
			timeEnd := "'" + inputDateEnd + " 23:59:59'"
			tx = tx.Where(`"createdAt" BETWEEN ? AND ?`, timeInit, timeEnd).Order(`"createdAt" desc`).Find(&c)
		}

		if len(c) <= 0 {
			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("htmltemplate1"))
			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("htmltemplate3"))
			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("htmltemplate2"))
			fmt.Println("Não há resultados para este filtro, tente rever os dados que inseriu.")
		} else {

			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("htmltemplate1"))
			if len(c) >= 20 {
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletemplate2"))
			} else {
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletemplate1"))
			}

			for i := 0; i < len(c); i++ {

				IDOrder := c[i].ID
				CardNumberOrder := c[i].CardNumber
				UserEmailOrder := c[i].UserEmail
				RechargeValueOrder := c[i].RechargeValue / 100
				CreatedAtOrder := c[i].CreatedAt
				KondutoStatus := c[i].KondutoStatus
				MundipaggID := c[i].MundipaggID
				var MundipaggWebhookIDOrder string
				if MundipaggWebhookIDOrder = c[i].MundipaggWebHookID; MundipaggWebhookIDOrder == "" {
					MundipaggWebhookIDOrder = "-"
				}
				var MundipaggIDOrder string
				if MundipaggIDOrder = c[i].MundipaggID; MundipaggIDOrder == "" {
					MundipaggIDOrder = "-"
				}
				var TacomIDOrder string
				if TacomIDOrder = c[i].TacomOrderID; TacomIDOrder == "" {
					TacomIDOrder = "-"
				}

				switch c[i].MundipaggStatus {
				case "order.payment_failed":
					MundipaggStatus = "Não pago"
				case "canceled":
					MundipaggStatus = "Pagamento cancelado"
				case "pending":
					MundipaggStatus = "Pendente"
				case "approved", "order.paid":
					MundipaggStatus = "Pago"
				default:
					MundipaggStatus = "-"
				}

				switch c[i].KondutoStatus {
				case "declined", "DECLINE":
					KondutoStatus = "Negado"
				case "canceled":
					KondutoStatus = "Cancelado"
				case "fraud":
					KondutoStatus = "Fraude"
				case "pending":
					KondutoStatus = "Pendente"
				case "approved", "APPROVE":
					KondutoStatus = "Aprovado"
				case "not_authorized":
					KondutoStatus = "Pagamento não autorizado"
				default:
					KondutoStatus = "-"
				}

				fmt.Fprintf(w, "%v \t", ordersearchtemplate.CCOrderSearchTemplate("tabletrbeggin"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", IDOrder)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", UserEmailOrder)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", CardNumberOrder)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", MundipaggID)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", TacomIDOrder)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%.2f \t", RechargeValueOrder)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", CreatedAtOrder.Format(dateFormat))
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", KondutoStatus)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdbeggin"))
				fmt.Fprintf(w, "%v \t", MundipaggStatus)
				fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletdend"))

				fmt.Fprintf(w, "\n")

			}

			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tablettend"))
			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("tabletemplateend"))
			fmt.Fprintf(w, "%v", ordersearchtemplate.CCOrderSearchTemplate("htmltemplate2"))

		}

		jsonData, _ := json.Marshal(c)
		excel.Create(jsonData)

	default:
		fmt.Fprintf(w, "Houve um problema ao tentar acessar a página solicitada, por gentileza entre em contato com o suporte pelo menu Sobre o Dashboard BHBus.")
	}

}
