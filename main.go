package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"

	siteabout   "dashboard-portfolio/dashboard/app/about"
	cardsearch  "dashboard-portfolio/dashboard/app/card"
	ordersearch "dashboard-portfolio/dashboard/app/order"
	opentickets "dashboard-portfolio/dashboard/app/tickets"
	usersearch  "dashboard-portfolio/dashboard/app/user"
)

func main() {

	http.HandleFunc("/", ordersearch.CCOrderSearch)
	http.HandleFunc("/user/", usersearch.Results)
	http.HandleFunc("/card/", cardsearch.CardSearchFunc)
	http.HandleFunc("/tickets/", opentickets.CreateNewTicket)
	http.HandleFunc("/about/", siteabout.AboutPage)

	//Verifica o sistema operacional para definir a rota
	switch os := runtime.GOOS; os {
	case "darwin", "windows":
		http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	case "linux":
		http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("opt/go/dashti/new-dashboard-bhbus/assets"))))
	default:
		http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	}

	fmt.Printf("Inicializando o servidor, acesse no navegador pela URL localhost:5005 \n")
	if err := http.ListenAndServe(":5005", nil); err != nil {
		log.Fatal(err)
	}

}
